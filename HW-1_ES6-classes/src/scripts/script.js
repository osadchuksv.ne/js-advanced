/*
ТЕОРЕТИЧНІ ПИТАННЯ
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
    

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
    Ключове слово super() потрібне для виклику конструктора батьківського класу та передачі йому параметру, а також для доступу до властивостей та методів батьківського класу.
*/ 

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set name(text) {
        return this._name = text;
    }
    get name() {
        return this._name;
    }

    set age(number) {
        return this._age = number;
    }
    get age() {
        return this._age;
    }

    set salary(number) {
        return this._salary = number;
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

const javaProgrammer = new Programmer("John", 25, 2000, "Java");
const pythonProgrammer = new Programmer("Alex", 22, 1500, "Python");
const rubyProgrammer = new Programmer("Jasper", 32, 3000, "Ruby");

console.log(javaProgrammer);
console.log(pythonProgrammer);
console.log(rubyProgrammer);


