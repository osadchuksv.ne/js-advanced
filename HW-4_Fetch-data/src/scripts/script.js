/*
ТЕОРЕТИЧНІ ПИТАННЯ
1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
  AJAX, або Asynchronous JavaScript And XML, це назва підходу або комплексу методів реалізації користувацьких інтерфейсів, при застосуванні яких дані овновлюються без перезавантаженгня сторінки. Перш за все цей підхід корисний тим, що для користувача це зручніше, так як зменшується споживання трафіку так як немає необхідності щоразу цілком завантажувати дані оновлюваної сторінки, відповідно зменшуєтсья і час очікування оновлення. Це особливо критично при повільному інтернеті. Зручніше це і чисто візуально, менше навантаження на сам рушій браузера. Також це дозволяє створювати візуально приємніші "безшовні" інтерфейси, в яких усе працює плавніше для користувача (уникаємо миготіння та зсування контенту, які неминуче викикають при повторному рендерингу DOM-дерева сторінки). Застосування асинхронності у JS дозволяє уникнути "call-back hell", внаслідок чого код стає читабельнішим, його простіше і створювати і підтримувати. Асинхронність, як головна фіча AJAX, дає нам змогу уникнути помилок в коді, коли ми не впевнені у тому чи прийдуть нам дані до моменту коли ми вже маємо їх використовувати. Як наслідок код може продовжувати виконуватись, очікуючи на потрібні дані, завдяки Промісам.
    
*/
const filmsList = document.querySelector(".sw-episodes");

fetch("https://ajax.test-danit.com/api/swapi/films")
.then(res => res.json())
.then(films => {
  films.forEach(film => {
    filmsList.insertAdjacentHTML("beforeend", 
    `<li class="episode"><h2>Epidode ${film.episodeId}</h2><h3>${film.name}</h3><p>${film.openingCrawl}<p></li><ul class=episode-${film.episodeId}_ch-list>Characters:</ul>`);
    let charList = document.querySelector(`.episode-${film.episodeId}_ch-list`);
    let loadingDIV = document.createElement("div");
    loadingDIV.innerHTML = "LOADING...";
    charList.append(loadingDIV);

    film.characters.forEach(chURL => {
      // симулюю зваантаження з сервера в 3 сек
      setTimeout(() => {
      fetch(chURL)
      .then(response => response.json())
      .then(ch => {
        loadingDIV.style.display = "none";
        charList.insertAdjacentHTML("beforeend", `<li class="character">${ch.name}</li>`);
      })
      .catch(err => {
        throw new Error(err);
      })
    }, 3000)
    })
      
  })

})
.catch(err => {
  console.error(err);
});
