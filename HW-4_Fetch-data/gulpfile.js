/*
const gulp = require("gulp");

const concat = require("gulp-concat");
const minify = require('gulp-minify');
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const browserSync = require('browser-sync').create();
const imagemin  = require('gulp-imagemin');
*/

import gulp from "gulp";
import concat from "gulp-concat";
import minify from 'gulp-minify';
import clean from 'gulp-clean';
import browserSync from 'browser-sync';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';

const sass = gulpSass(dartSass);


const html = () => {
    return gulp.src("./src/*.html")
        .pipe(gulp.dest("./dist"));
}


const js = () => {
    return gulp.src("./src/scripts/**/*.js")
        .pipe(concat('script.js'))
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.min.js'
            },
        }))
        .pipe(gulp.dest("./dist/scripts"));
}

const cleanDist = () => {
    return gulp.src('./dist', {read: false})
        .pipe(clean());
}

const server = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
}

const watcher = () => {
    gulp.watch("./src/*.html", html).on('all', browserSync.reload);
    gulp.watch("./src/scripts/**/*.js", js).on('all', browserSync.reload);
}


gulp.task("html", html);
gulp.task("script", js);
gulp.task("cleanDist", cleanDist);
gulp.task("browser-sync", server);


gulp.task("build", gulp.series(
    cleanDist,
    gulp.parallel(html, js)
));

gulp.task("dev", gulp.series(
    gulp.parallel(html, js),
    gulp.parallel(server, watcher)
))
