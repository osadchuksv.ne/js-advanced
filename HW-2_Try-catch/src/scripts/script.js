/*
ТЕОРЕТИЧНІ ПИТАННЯ
1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
    Конструкцію доречно використовувати у випадку, коли нам потрібно, щоб навіть у разі, якщо станеться помилка, код все одно продовжив виконуватись і не зламав увесь скрипт.
    Також, в теорії, можемо використати для глибшої класифікації помилки, ніж просто за типом, завдяки оброці типової помилки в блоці catch. Ще ми можемо створити власну "кастомну" помилку, що збільшить поінформованість розробника про причину неочікуваного результату та допоможе відлагодити логіку, не ламаючи її. При деякій обробці ми також можемо детальніше сповіщати користувача про суть помилки, що стається.

*/
// "use strict";

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const targetDiv = document.createElement("div");
targetDiv.style.id = "root";
document.body.append(targetDiv);
const booksList = document.createElement("ul");

try {
    let errorArr = [];
    books.forEach((elem) => {
        let elemValues = Object.values(elem);
        let elemKeys = Object.keys(elem);
        let elemEntries = Object.entries(elem);
        const authorKey = "author";
        const nameKey = "name";
        const priceKey = "price";

    if(elemKeys.length >= 3) {
        let listItem = document.createElement("li");
        listItem.innerText = `${elem.name} - автор ${elem.author}. Ціна - ${elem.price}$`;
        booksList.append(listItem);
        targetDiv.append(booksList);

    }else{
        if(!elem.author){
            errorArr.push(`${authorKey} is missing for book ${elem.name}`);
        }
        if(!elem.name){
            errorArr.push(`${nameKey} is missing for book ${elem.name}`);
        }
        if(!elem.price){
            errorArr.push(`${priceKey} is missing for book ${elem.name}`);
        }
    }

    })

    targetDiv.append(booksList);
    throw errorArr;
    
} catch (e) {
    e.forEach((element) => {
    console.error(element);
})
}
