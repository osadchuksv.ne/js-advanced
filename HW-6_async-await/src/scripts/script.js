/*
ТЕОРЕТИЧНЕ ПИТАННЯ 
1. Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.
  Питання дуже схоже до питання з попередньої ДЗ:) Суть асинхронності у тому, що ми можемо писати код, адаптуючи його до того, що якісь операції та запити можуть займати певний час на своє виконання. Тобто використовуючи асинхронність ми можемо не переживати, що на момент взаємодії із змінною в ній може ще не бути потрібних данних, що не встигне пройти якийсь запит. Це якщо коротко і по суті.

*/

const getIpURL = "https://api.ipify.org/?format=json";
const getInfoURL = "http://ip-api.com/json/";

async function getIpInfo(URL) {
  try {
    let result = await fetch(URL).then(res => res.json());
    let { ip } = result;

    let ipInfo = await fetch(`${getInfoURL}${ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,query`)
      .then(res => res.json());

    let { continent, country, city, regionName } = ipInfo;

    const details = document.createElement("div");
    details.classList.add("ip-info")
    details.insertAdjacentHTML("beforeend",
      `<p class="ip-info__header">Continent: <span class="ip-info__value">${continent}</span></p>
      <p class="ip-info__header">Country: <span class="ip-info__value">${country}</span></p>
      <p class="ip-info__header">Region: <span class="ip-info__value">${regionName}</span></p>
      <p class="ip-info__header">City: <span class="ip-info__value">${city}</span></p>
      `
    )
    const container = document.querySelector(".container")
    container.append(details);
    details.style.display = "flex";
  }
  catch (err) {
    console.error(err);
  }
}

document.querySelector(".ip-find-btn").addEventListener("click", () => {
  getIpInfo(getIpURL);
});


