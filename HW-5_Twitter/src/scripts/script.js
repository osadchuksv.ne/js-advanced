/* 
Creating class for cards creating and rendering 
*/ 

class Card {
  constructor(author, email, title, body, id) {
    this.title = title;
    this.body = body;
    this.author = author;
    this.email = email;
    this.id = id;
  }

  render() {
    const post = document.createElement("div");
    post.classList.add("twitt__card-wrapper", `id-${this.id}`);
    post.insertAdjacentHTML("beforeend", `
    <span class="twitt__edit-hint">Click the text you'd like to edit</span>
    <p class="twitt__author">${this.author}</p>
    <button data-card-id="${this.id}" class="twitt__edit-btn"><img class="pencil-ico" src="/images/edit_modify_icon-72390.png"></button>
    <button data-card-id="${this.id}" class="twitt__delete-btn"><img class="garbage-ico" src="/images/icons-delete.svg"></button>
    <span class="twitt__author-email">${this.email}</span>
    <p data-card-id="${this.id}" class="twitt__title" contenteditable="false">${this.title}</p>
    <p data-card-id="${this.id}" class="twitt__text" contenteditable="false">${this.body}</p>
    <button class="twitt__edit-save-btn">Save</button>
    <button class="twitt__edit-cancel-btn">Cancel</button>
    `)
    document.body.append(post);
  }

// Further is just another idea for editing and deleting cards which hadn't been made a working solution. You can just skip them as they are not used:

  // edit() {
  //   fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
  //     method: "PUT",
  //     body: JSON.stringify({
  //       title: '',
  //       body: 'John',
  //     }),
  //     headers: {
  //       'Content-Type': 'application/json'
  //     }
  //   })
  //     .then(response => response.json())
  //     .then(json => console.log(json))
  //     .catch(err => {
  //       console.error(err);
  //     })
  // }

  // delete() {
  //   fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
  //     method: "DELETE",
  //   })
  //     .catch(err => {
  //       console.error(err);
  //     })
  // }
}

// Sending a fetch to the server to get post:
fetch("https://ajax.test-danit.com/api/json/posts")
  .then(res => res.json())
  .then(posts => {
    fetch("https://ajax.test-danit.com/api/json/users")
      .then(res => res.json())
      .then(users => {
        // console.log(users);
        // console.log(posts);
        // CARD CREATING! Seraching for matches between post and users, creating and rendering cards:
        posts.forEach(post => {
          let { name: author, email } = users.find(el => el.id === post.userId);
          let postCard = new Card(author, email, post.title, post.body, post.id);
          postCard.render();
        })
      })
      // One of possible solutions - going ahead with whole programm logic into ".then":
      .then(() => {
        document.querySelectorAll(".twitt__card-wrapper").forEach((elem) => {
          // create variables here for not to force them renew on every event assigned to 'elem':
          let titlePrimary;
          let textPrimary;
          /* THE MAIN LOGIC STARTS HERE. Creating event listener over enumerable elems ('div's or cards in other words) and delegating events over the card.
              Next we're detecting target element and tell what to do then.
          */
          elem.addEventListener("click", (event) => {
            let target = event.target;
            // write down the card Id for later fetches:
            let cardID = elem.classList[1].slice(3);
            // locate the current card 'div' to iteract with it and its child elements (twitter post title, twitter post content or text):
            let twitt = target.closest("div");
            let twittTitle = twitt.querySelector(".twitt__title");
            let twittText = twitt.querySelector(".twitt__text");
            let editHint = twitt.querySelector(".twitt__edit-hint");
            let editSaveBtn = twitt.querySelector(".twitt__edit-save-btn");
            let editCancelBtn = twitt.querySelector(".twitt__edit-cancel-btn");
            // Simple - we click the garbage icon and delete the post:
            if (target.className === "garbage-ico") {
              fetch(`https://ajax.test-danit.com/api/json/posts/${cardID}`, {
                method: "DELETE",
              })
              .then(({ status }) => {
                if (status === 200) {
                  target.closest("div").remove();
                }
              })
              .catch(err => {
                console.error(err);
              })
              // POST EDITING! Here we start the big part of the code dedicated to post changing! We click 'edit' icon (pic of the pencil in the card):
            } else if (target.className === "pencil-ico") {
              // we're saving the initial text of the editable elements of twitter post to previously declared variables: 
              titlePrimary = twittTitle.innerText;
              textPrimary = twittText.innerText;
              // showing hint for the user as it's not obvious that user should click on the element to start editing it:
              editHint.style.display = "initial";
              // ONLY 2 ELEMENTS are allowed to be changed - post title and main text! Changing author or email should be made with some other interface:
              twittTitle.setAttribute("contenteditable", "true");
              twittText.setAttribute("contenteditable", "true");
            }
              // We check if the edit icon was clicked before (if "contenteditable" attribute is set to "true" - it has been clicked):
            if ((event.target === twittTitle || event.target === twittText)  && twittTitle.getAttribute("contenteditable") === "true") {
              editHint.style.display = "none";
              editSaveBtn.style.display = "initial";
              editCancelBtn.style.display = "initial";
            }
            if(event.target === editSaveBtn){
                editSaveBtn.style.display = "none";
                editCancelBtn.style.display = "none";
                twittTitle.setAttribute("contenteditable", "false");
                twittText.setAttribute("contenteditable", "false");
                // we put changed text of the elements into variables to send them to server:
                let titleChanged = twittTitle.innerText;
                let textChanged = twittText.innerText;
                fetch(`https://ajax.test-danit.com/api/json/posts/${cardID}`, {
                  method: "PUT",
                  body: JSON.stringify({
                    title: `${titleChanged}`,
                    body: `${textChanged}`
                  }),
                  headers: {
                    'Content-Type': 'application/json'
                  }
                })
                // Working with response from the server onto user changes:
                .then(({ status }) => {
                  let hintPrimaryText = editHint.innerText;
                  if (status === 200){
                    editHint.innerText = "Changes have been successfully saved!";
                    editHint.style.display = "initial";
                    setTimeout(() => {
                      editHint.style.display = "none";
                      editHint.innerText = hintPrimaryText;
                    }, 3000)
                  } else {
                    editHint.style.color = "red";
                    editHint.innerText = "Something went wrong. Changes haven't been saved!";
                    editHint.style.display = "initial";
                    setTimeout(() => {
                      editHint.style.display = "none";
                      editHint.innerText = hintPrimaryText;
                      editHint.style.color = "lightgreen";

                    }, 3000)
                  }
                })
                .catch(err => {
                  console.error(err);
                })
             // we decline changes from user returning card appearance to initial
            }else if(event.target === editCancelBtn){
              editSaveBtn.style.display = "none";
              editCancelBtn.style.display = "none";
              twittTitle.setAttribute("contenteditable", "false");
              twittText.setAttribute("contenteditable", "false");
              twittTitle.innerText = titlePrimary;
              twittText.innerText = textPrimary;
            }
          })
        })
      })
  })
  .catch(err => {
    console.error(err);
  })
.catch (err => {
  console.error(err);
})